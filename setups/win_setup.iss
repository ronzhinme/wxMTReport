#define BinPath "..\bin"
#define SrcPath "..\src"
#define LibPath "..\lib"
#define ImgPath "..\img"

#define MyAppVersion GetFileVersion( "{#BinPath}\lightReport.exe" )
#define MyAppName "��������� �������� lightReport"
#define MyAppPublisher ""
#define MyAppURL "http://c-help.tk"
#define MyAppExeName "lightReport.exe"
#define IcoPath "{app}\img\lightReport.ico"
[Setup]
AppId={{545A709C-32A5-48A1-9375-CC7758F38BE0}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
AppVerName={#MyAppName} {#MyAppVersion}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
DefaultDirName={pf}\lightReport
DefaultGroupName=lightReport
DisableReadyPage=yes
DisableWelcomePage=yes
DisableProgramGroupPage=yes 
PrivilegesRequired=none
OutputDir=./
OutputBaseFilename=lightReport
Compression=lzma
SolidCompression=yes
ShowLanguageDialog=no
UninstallDisplayIcon={#IcoPath}
LicenseFile= {#SrcPath}\license.txt
[Languages]
Name: "russian"; MessagesFile: "compiler:Languages\Russian.isl"

[Types]
Name: "full" ; Description: "������ ���������"
Name: "repo" ; Description: "������ ��������� ��������"
Name: "clnt" ; Description: "������ ���������� ����������"

[Components]
Name: "full"; Description: "��"; Types: full
Name: "repo"; Description: "������ ���������� ��������"; Types: repo
Name: "clnt"; Description: "������ ���������� �����"; Types: clnt 


[Files]
Source: "{#BinPath}\lightReport.exe"; DestDir: "{app}"; Flags: ignoreversion; Components: full repo
Source: "{#SrcPath}\lang\ru\*"; DestDir: "{app}\ru"; Components: full repo

Source: "{#LibPath}\zlib128-dll\zlib1.dll"; DestDir: "{app}"; Components: full repo clnt

Source: "{#BinPath}\repclient.exe"; DestDir: "{app}"; Flags: ignoreversion; Components: full clnt
Source: "{#SrcPath}\wxMTReport_lib.h"; DestDir: "{app}"; Flags: ignoreversion; Components: full clnt
Source: "{#SrcPath}\repclient_main.cpp"; DestDir: "{app}"; Flags: ignoreversion; Components: full clnt
Source: "{#BinPath}\lightReportLib.dll"; DestDir: "{app}"; Flags: ignoreversion; Components: full clnt

Source: "{#ImgPath}\*"; DestDir: "{app}\img"; Components: full repo

 

[Icons]
Name: "{group}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"
Name: "{group}\{cm:ProgramOnTheWeb,{#MyAppName}}"; Filename: "{#MyAppURL}"
Name: "{group}\{cm:UninstallProgram,{#MyAppName}}"; Filename: "{uninstallexe}"
Name: "{commondesktop}\lightReport\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; 

[Run]
Filename: "{app}\{#MyAppExeName}"; Description: "{cm:LaunchProgram,{#StringChange(MyAppName, '&', '&&')}}"; Flags: nowait postinstall skipifsilent; Components: full repo

[UninstallDelete]
Type: files; Name: "{app}\conf.dat"; Components: full repo
Type: dirifempty; Name: "{app}"

[Code]
procedure RegistryModifyAsRoot();
begin
  RegWriteStringValue(HKEY_CLASSES_ROOT,'.mrp','', 'LightReport.MainReportProject');
  RegWriteStringValue(HKEY_CLASSES_ROOT,'LightReport.MainReportProject','', '������ LightReport');
  RegWriteStringValue(HKEY_CLASSES_ROOT,'LightReport.MainReportProject\DefaultIcon','', ExpandConstant('{#IcoPath}'));
  RegWriteStringValue(HKEY_CLASSES_ROOT,'LightReport.MainReportProject\shell\open','', '�������');
  RegWriteStringValue(HKEY_CLASSES_ROOT,'LightReport.MainReportProject\shell\open\command','', '"'+ExpandConstant('{app}')+'\'+ExpandConstant('{#MyAppExeName}')+'" "%1"');
end;

function GetCustomSetupExitCode: Integer;
begin
    if (IsAdminLoggedOn()) then 
  begin
    RegistryModifyAsRoot();
    result:=0;
  end;
end;
