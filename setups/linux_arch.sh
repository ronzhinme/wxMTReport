#!/bin/sh

LREP=lightreport.tar
CDIR=`pwd`
TDIR=~/test/

cp -rf ../img/ $TDIR/bin/
cp -rf ../src/lang/* $TDIR/bin
cp -rf $TDIR/* $CDIR
cp ../src/repclient_main.cpp $CDIR/lib
cp ../src/wxMTReport_lib.h $CDIR/lib

tar -cf $LREP ./bin ./lib
gzip -f9 $LREP

rm -fr ./bin ./lib 





