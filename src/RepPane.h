
#ifndef REPPANE_H
#define REPPANE_H

#include <wx/scrolwin.h>
#include <wx/dc.h>
#include <wx/vector.h>

#include "ReportPrinter.h"
#include "./ObjModelLib/RepProject.h"
using namespace ElementModel;

wxDECLARE_EVENT(CHPROPVALUE_EVENT, wxCommandEvent);
wxDECLARE_EVENT(SELELEMENT_EVENT, wxCommandEvent);
class RepPane: public wxScrolledWindow
{
private:
  enum MouseAction {maMove, maDrag};
  MouseAction  mousestate_;
  Doc* cur_doc_;
  Element* cur_el_;

  wxPoint mouse_lbtn_click_pos_;
  wxPoint  curmousepos_;

  wxPoint* selpoint_;
  int pointDirection_;

  void DrawSelectedElementMarkers(wxDC* dc);
  void OnMouseDrag(wxMouseEvent& evt);
  void OnMouseLBtnDown(wxMouseEvent& evt);
  void OnMouseLBtnUp(wxMouseEvent& evt);
  void ChangePositionPropertyValue();
  bool IsInElementRegion(wxPoint &pos, Element* el);
  void SelectElement(Element* el);
  void GetSelPointAndDirection(Element* el);
  long GetSizeScale();
  bool CheckDrag() const;
  void FindAndSelectElement();
  void GetTopDownPoints(Element* el, wxPoint& lt, wxPoint& rb);
  wxDECLARE_EVENT_TABLE();
  virtual void OnDraw(wxDC& dc);
public:


  RepProj* repproj;
  RepPane(wxWindow* parent,
          wxWindowID winid = wxID_ANY,
          const wxPoint& pos = wxDefaultPosition,
          const wxSize& size = wxDefaultSize,
          const wxString& name = wxPanelNameStr);

  void SetPageId(long val);
  void ShowElementMarkers(long val);
  void ScrollToSelectedElement();
  void Repaint();
};



#endif // REPPANE_H
