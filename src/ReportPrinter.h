#ifndef REPORTPRINTER_H
#define REPORTPRINTER_H

#include <wx/print.h>
#include <wx/printdlg.h>
#include <wx/vector.h>

#include "./ObjModelLib/RepProject.h"
#include "./ObjModelLib/defines.h"
using namespace ElementModel;

enum VisibilityType
{
  MODEL=1,
  REPORT=2,
};

class RepPrinter:public wxPrintout
{
private:
  RepProj* repproj_;
  wxPen* curpen_;
  wxBrush* curbrush_;
  wxDC* curdc_;
  int page_num_;
  int totalpages_;
  Doc* doc_;
  long visible_flag_;
  void FitDocToPage();
  double GetScaleVal(double v1, double v2);

  void GetVariablesValue(wxString& text);
  void DrawLine(Element* elem);
  void DrawRect(Element* elem);
  void DrawText(Element* elem);
  void DrawImage(Element* elem);

  wxPoint GetPoint(Element* elem, bool is_topleft=true);
  wxSize GetSize(Element* elem);
  void SetLineSettings(Element* elem);
  void SetBrushSettings(Element* elem);
  void SetLineColour(Element* elem);
  void SetLineType(Element* elem);
  void SetLineWeight(Element* elem);
  void SetBrushColour(Element* elem);
  void SetBrushType(Element* elem);
  void SetFont(Element* elem);
  void WrapText(wxString& text, int maxwidth, long angle);
  void PrintPaneMargin();
public:
  RepPrinter(VisibilityType vistyp, wxDC* dc);
  ~RepPrinter() {}

  virtual bool OnPrintPage(int page);
  virtual bool HasPage(int page);
  virtual void GetPageInfo (int* minPage, int* maxPage, int* pageFrom, int* pageTo);
  void PrintDocument(int page=-1);
  bool SetRepProjectObject(RepProj* repproj, int page_id);
};

#endif // REPORTPRINTER_H
