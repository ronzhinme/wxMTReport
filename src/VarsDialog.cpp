#include "VarsDialog.h"
#include "./ObjModelLib/defines.h"

//(*InternalHeaders(VarsDialog)
#include <wx/intl.h>
#include <wx/string.h>
//*)

//(*IdInit(VarsDialog)
const long VarsDialog::ID_GRID1 = wxNewId();
const long VarsDialog::ID_BTNCANCEL = wxNewId();
const long VarsDialog::ID_BTNAPPLY = wxNewId();
//*)

BEGIN_EVENT_TABLE(VarsDialog,wxDialog)
  //(*EventTable(VarsDialog)
  //*)
END_EVENT_TABLE()

VarsDialog::VarsDialog(wxWindow* parent,wxWindowID id,const wxPoint& pos,const wxSize& size)
{
  //(*Initialize(VarsDialog)
  wxBoxSizer* BoxSizer1;
  wxBoxSizer* BoxSizer3;

  Create(parent, id, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE, _T("id"));
  SetClientSize(wxSize(200,400));
  Move(wxDefaultPosition);
  BoxSizer1 = new wxBoxSizer(wxVERTICAL);
  Grid1 = new wxGrid(this, ID_GRID1, wxDefaultPosition, wxDefaultSize, wxWANTS_CHARS, _T("ID_GRID1"));
  Grid1->CreateGrid(1,1);
  Grid1->EnableEditing(true);
  Grid1->EnableGridLines(true);
  Grid1->SetDefaultCellFont( Grid1->GetFont() );
  Grid1->SetDefaultCellTextColour( Grid1->GetForegroundColour() );
  BoxSizer1->Add(Grid1, 1, wxALL|wxEXPAND|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
  BoxSizer3 = new wxBoxSizer(wxHORIZONTAL);
  Btn_Cancel = new wxButton(this, ID_BTNCANCEL, _("Cancel"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_BTNCANCEL"));
  BoxSizer3->Add(Btn_Cancel, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
  Btn_Apply = new wxButton(this, ID_BTNAPPLY, _("Apply"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_BTNAPPLY"));
  BoxSizer3->Add(Btn_Apply, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
  BoxSizer1->Add(BoxSizer3, 0, wxALL|wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL, 5);
  SetSizer(BoxSizer1);
  SetSizer(BoxSizer1);
  Layout();

  Connect(ID_GRID1,wxEVT_GRID_CELL_CHANGE,(wxObjectEventFunction)&VarsDialog::OnGrid1CellChange);
  Grid1->Connect(wxEVT_KEY_UP,(wxObjectEventFunction)&VarsDialog::OnKeyUp,0,this);
  Connect(ID_BTNCANCEL,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&VarsDialog::OnButtonEndDlgClick);
  Connect(ID_BTNAPPLY,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&VarsDialog::OnButtonEndDlgClick);
  //*)
  Init();
}

void VarsDialog::Init()
{
  Grid1->SetColLabelValue(0,PGPAGE_VARS);
}

VarsDialog::~VarsDialog()
{
  //(*Destroy(VarsDialog)
  //*)
}

void VarsDialog::OnGrid1CellChange(wxGridEvent& event)
{
  rowDeleting_=false;
  int total_rows_count = Grid1->GetNumberRows();
  for(int f=0;f<total_rows_count;++f)
  {
    if(Grid1->GetCellValue(f,0)!=wxEmptyString && f==total_rows_count-1)
    {
      Grid1->AppendRows(1);
      Grid1->MoveCursorDown(false);
      total_rows_count = Grid1->GetNumberRows();
    }
    else if(Grid1->GetCellValue(f,0)==wxEmptyString && f!=total_rows_count-1)
    {
      rowDeleting_=true;
      Grid1->DeleteRows(f);
      total_rows_count = Grid1->GetNumberRows();
    }
  }
}

void VarsDialog::OnKeyUp(wxKeyEvent& event)
{
  if(rowDeleting_ && event.GetKeyCode()==WXK_RETURN)
  {
    Grid1->MoveCursorUp(false);
    Grid1->MoveCursorUp(false);
    rowDeleting_=false;
  }
}


void VarsDialog::OnButtonEndDlgClick(wxCommandEvent& event)
{
  if(event.GetId()==ID_BTNCANCEL)
  {
    EndModal(wxID_CANCEL);
  }
  if(event.GetId()==ID_BTNAPPLY)
  {
    EndModal(wxID_APPLY);
  }
}

void VarsDialog::SetVars(wxVector<Var> vars)
{
  vars_.clear();
  vars_.assign(vars.begin(), vars.end());

  Grid1->ClearGrid();
  Grid1->AppendRows(vars_.size());
  int f=0;
  for(wxVector<Var>::iterator i=vars_.begin(); i!=vars_.end();++i)
  {
    Grid1->SetCellValue(i->name,f,0);
    ++f;
  }
}
wxVector<Var> VarsDialog::GetVars()
{
  vars_.clear();
  for(int f=0;f<Grid1->GetNumberRows();++f)
  {
    wxString name = Grid1->GetCellValue(f,0);
    if(name==wxEmptyString)continue;
    Var v(name);
    vars_.push_back(v);
  }
  return vars_;
}
