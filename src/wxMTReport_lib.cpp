
//#include <wx/wx.h>
//#include <wx/encconv.h>

#include "./ObjModelLib/RepProject.h"
using namespace ElementModel;

#include "ReportPrinter.h"
#include "wxMTReport_lib.h"


wxPGChoices chs_;

RepProj repproj;
long page_id=-1;
char is_projectloaded=false;


class wxDLLInitializer: public wxApp
{
private:
  int call_count;
public:
  wxDLLInitializer() {call_count=0;}
  ~wxDLLInitializer()
  {
//    for(int f=0; f<call_count; ++f)
//    {
//      wxUninitialize();
//    }
  }
  virtual bool OnInit();
};
IMPLEMENT_APP(wxDLLInitializer)
bool wxDLLInitializer::OnInit()
{
  if(wxInitialize())
  {
    ++call_count;
    return true;
  }
  return false;
}
wxDLLInitializer dll; //без этого GUI элементы и зависимые части уронят программу


bool OpenReport(const char* filename)
{
  dll.OnInit();
  wxString fn1 = wxString(filename);
  is_projectloaded = repproj.LoadProject( fn1 );

  return is_projectloaded;
}
long GetPageCount()
{
  if(!is_projectloaded)
  {
    return -1;
  }
  return repproj.docs.GetCount();
}
const char*  GetPageName(long indx)
{
  if(!is_projectloaded)
  {
    return NULL;
  }
  Doc* doc = repproj.docs.Get(indx,true);
  if(doc==NULL)
  {
    return NULL;
  }
  return doc->GetName();
}
bool SetCurrentDocument(long indx)
{
  page_id=-1;
  if(!is_projectloaded)
  {
    return false;
  }
  Doc* doc = repproj.docs.Get(indx,true);
  if(doc==NULL)
  {
    return false;
  }
  page_id = doc->id;
  return true;
}
long GetVarCount()
{
  if(!is_projectloaded)
  {
    return -1;
  }
  return repproj.vars.GetVars().size();
}
const char*   GetVarVal(long indx)
{
  if(indx<0 || indx>=(long)repproj.vars.GetVars().size()) { return NULL; }
  return *(repproj.vars.GetVars().at(indx).value);
}
const char* GetVarName(long indx)
{
  if(indx<0 || indx>=(long)repproj.vars.GetVars().size()) { return NULL; }
  return repproj.vars.GetVars().at(indx).name;
}
bool SetVarVal(long indx,char* val)
{
  if(indx<0 || indx>=(long)repproj.vars.GetVars().size()) { return false; }
  Var var = repproj.vars.GetVars().at(indx);

  var.SetVal(wxString(val));
  return true;
}
void PrintPage()
{
  if(!is_projectloaded || page_id==-1)
  {
    return;
  }

  RepPrinter* print = new RepPrinter(REPORT,NULL);
  if(!print->SetRepProjectObject(&repproj,page_id)) {return;}

  Doc* doc = repproj.docs.Get(page_id,false);
  if(doc==NULL)
  {
    return;
  }
  doc->print_dt.SetPaperId(wxPAPER_NONE);
  doc->print_dt.SetPaperSize(doc->print_dt.GetPaperSize() );

  wxPrinter prntr;
  prntr.Print(NULL ,print);
}
