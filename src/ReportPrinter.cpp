#include <wx/file.h>
#include <wx/cmndata.h>
#include <wx/image.h>
#include "ReportPrinter.h"
#include "DrawedComboProperty.h"

RepPrinter::RepPrinter(VisibilityType vistyp, wxDC* dc)
{
  doc_=NULL;
  curdc_=dc;
  repproj_=NULL;
  curpen_ = new wxPen(* wxBLACK_PEN);
  curbrush_ = new wxBrush(* wxWHITE_BRUSH);
  visible_flag_=vistyp;
}
bool RepPrinter::SetRepProjectObject(RepProj* repproj, int page_id)
{
  repproj_=repproj;
  doc_ = repproj_->docs.Get(page_id,false);
  if(doc_==NULL)
    {
      return false;
    }
  return true;
}
bool RepPrinter::OnPrintPage(int page)
{
  if(!HasPage(page))
    {
      return false;
    }
  PrintDocument(page);
  return true;
}
void RepPrinter::GetPageInfo (int* minPage, int* maxPage, int* pageFrom, int* pageTo)
{
  totalpages_ = 1;
  *minPage=1;
  *pageFrom=1;
  *pageTo=*maxPage=totalpages_;
}
bool RepPrinter::HasPage(int page)
{
  return page<=totalpages_;
}
double RepPrinter::GetScaleVal(double v1, double v2)
{
  if(v1>v2)
    {
      return v1/v2;
    }
  else
    {
      return v2/v1;
    }
}
void RepPrinter::FitDocToPage()
{
  if(!curdc_)
    {
      return;
    }

  wxRect p_rect = GetLogicalPaperRect();
  wxRect pg_rec = GetLogicalPageRect();

  int kw = p_rect.width - (p_rect.width - pg_rec.width);
  int kh =  p_rect.height - (p_rect.height - pg_rec.height);

  wxSize docsz = doc_->print_dt.GetPaperSize();
  double kx = GetScaleVal(docsz.GetWidth(),kw);
  double ky = GetScaleVal(docsz.GetHeight(),kh);
  curdc_->SetUserScale(kx,ky);
}
void RepPrinter::PrintPaneMargin()
{
  if(!doc_)
    {
      return;
    }
  curdc_->SetPen(*wxBLACK_DASHED_PEN);
  curdc_->SetMapMode(wxMM_LOMETRIC);
  curdc_->DrawRectangle( *(new wxPoint(0,0)), doc_->sizemm);
}
void RepPrinter::PrintDocument(int page)
{
  if(doc_==NULL)
    {
      return;
    }
  if(curdc_==NULL)
    {
      curdc_=GetDC();
    }
  if(curdc_==NULL)
    {
      return;
    }

  curdc_->Clear();
  curdc_->SetMapMode(wxMM_LOMETRIC);
  if(visible_flag_==REPORT)
    {
      FitDocToPage();
    }

  PrintPaneMargin();

#ifdef LOG
  wxString msg;
#endif // LOG
  if(doc_->elems.GetCount()==0)
  {
    return;
  }

  bool just_printed_elemens[doc_->elems.GetCount()];

  for(long layer = 0 ; layer< 10; ++layer)
    {
      for(long f=0; f<doc_->elems.GetCount(); ++f)
        {
          Element* el = doc_->elems.Get(f);
          PropertyData* dt = el->prop.GetProperty(PROP_VISIBLE_ID);
          if(dt==NULL)
            {
              continue;
            }
          long cur_visible = dt->value.GetLong();
          if( (cur_visible & visible_flag_)==0)
            {
              continue;
            }

          PropertyData* layer_data = el->prop.GetProperty(PROP_LAYER_ID);
          if(layer_data==NULL || layer_data->value.GetLong() != layer || just_printed_elemens[f] == true)
          {
            continue;
          }

          just_printed_elemens[f]=true;

          switch(el->GetType())
            {
            case EL_LINE:
              SetLineType(el);
              SetLineColour(el);
              SetLineWeight(el);
              DrawLine(el);
#ifdef LOG
              msg = _T("DrawLine\n");
#endif // LOG
              break;
            case EL_RECT:
              DrawRect(el);
              break;
            case EL_TEXT:
              DrawText(el);
#ifdef LOG
              msg=_T("DrawText\n");
#endif // LOG
              break;
            case EL_IMAGE:
              DrawImage(el);
#ifdef LOG
              msg=_T("DrawImage\n");
#endif // LOG
              break;
            case EL_UNKNOWN:
            case EL_NULL:
            case EL_PAGE:
              break;
            }
#ifdef LOG
          WRITELOG(msg);
#endif // LOG
          // TODO этот кусок кода вызывается каждый раз, даже если изменений параметров не было. нужно оптимизировать, чтоб вызывался только при изменении значений изменяющих видимость параметров
        }
    }
}

// ===============================================
// Additional Methods
void RepPrinter::SetLineSettings(Element* elem)
{
  SetLineColour(elem);
  SetLineType(elem);
  SetLineWeight(elem);
}
void RepPrinter::SetBrushSettings(Element* elem)
{
  SetBrushColour(elem);
  SetBrushType(elem);
}
void RepPrinter::SetLineColour(Element* elem)
{
  PropertyData* p=elem->prop.GetProperty(PROP_PENCLR_ID);
  if(p==NULL || curdc_==NULL)
    {
      return;
    }

  wxColour colour;
  p->GetColour(colour);
  curpen_->SetColour(colour);
}
void RepPrinter::SetLineType(Element* elem)
{
  PropertyData* p=elem->prop.GetProperty(PROP_PENTYP_ID);
  if(p==NULL || curdc_==NULL)
    {
      return;
    }

  long ltyp = p->value.GetLong();
  curpen_->SetStyle(PenStyleComboProperty::GetPenStyle(ltyp));
}
void RepPrinter::SetLineWeight(Element* elem)
{
  PropertyData* p=elem->prop.GetProperty(PROP_PENWEIGHT_ID);
  if(p==NULL || curdc_==NULL)
    {
      return;
    }


  int weight = p->value.GetLong();
  int pweight = curpen_->GetWidth();
  if(weight==pweight)
    {
      return;
    }

  curpen_->SetWidth(weight);
}
void RepPrinter::SetBrushColour(Element* elem)
{
  PropertyData* p=elem->prop.GetProperty(PROP_FILLCLR_ID);
  if(p==NULL || curdc_==NULL)
    {
      return;
    }

  wxColour colour;
  p->GetColour(colour);
  curbrush_->SetColour(colour);
}
void RepPrinter::SetBrushType(Element* elem)
{
  PropertyData* p=elem->prop.GetProperty(PROP_FILLTYP_ID);
  if(p==NULL || curdc_==NULL)
    {
      return;
    }

  long ltyp = p->value.GetLong();
  curbrush_->SetStyle(FillStyleComboProperty::GetBrushStyle(ltyp));
}
wxSize RepPrinter::GetSize(Element* elem)
{
  wxSize sz;
  double x,y;
  PropertyData* p = elem->prop.GetProperty(PROP_WIDTH_ID);
  if(p==NULL)
    {
      return sz;
    }
  x=p->value.GetDouble();
  p = elem->prop.GetProperty(PROP_HEIGHT_ID);
  if(p==NULL)
    {
      return sz;
    }
  y=p->value.GetDouble();
  double kf = doc_->prop.GetProperty(PROP_SIZEMETER_ID)->value.GetLong();
  sz.Set((int)(x*kf),(int)(y*kf));

  return sz;
}
wxPoint RepPrinter::GetPoint(Element* elem, bool is_topleft)
{
  wxPoint pnt;
  PropertyData* p = NULL;
  double x,y;
  if(is_topleft)
    {
      p=elem->prop.GetProperty(PROP_POSLEFT_ID);
      if(p==NULL)
        {
          return pnt;
        }
      x = p->value.GetDouble();
      p= elem->prop.GetProperty(PROP_POSTOP_ID);
      if(p==NULL)
        {
          return pnt;
        }
      y = p->value.GetDouble();
    }
  else
    {
      p=elem->prop.GetProperty(PROP_POSRIGHT_ID);
      if(p==NULL)
        {
          return pnt;
        }
      x = p->value.GetDouble();
      p= elem->prop.GetProperty(PROP_POSBOTTOM_ID);
      if(p==NULL)
        {
          return pnt;
        }
      y = p->value.GetDouble();
    }
  double kf = doc_->prop.GetProperty(PROP_SIZEMETER_ID)->value.GetLong();
  pnt.x=(int)(x*kf);
  pnt.y=(int)(y*kf);
  return pnt;
}
void RepPrinter::GetVariablesValue(wxString& text)
{
  if(repproj_==NULL)
    {
      return;
    }

  wxVector<Var> vars = repproj_->vars.GetVars();
  for(wxVector<Var>::iterator i = vars.begin(); i!=vars.end(); i++)
    {
      Var v = *i;
      wxString var_ref = _T("${")+v.name+_T("}$");
      if(text.Find(var_ref)!=wxNOT_FOUND)
        {
          if(v.value->IsSameAs(wxEmptyString))
            {
              if(visible_flag_==MODEL)
                {
                  v.SetVal(_("EmptyValue"));
                }
            }
          text.Replace(var_ref,*v.value);
        }
    }
}
void RepPrinter::WrapText(wxString& text, int maxwidth, long angle)
{
  wxCoord chw = curdc_->GetCharWidth(); //1 char width (aveage)
  double rad = angle*0.0175;
  double cos_rad = cos(rad);

  int char_cnt = 0;
  if(cos_rad!=0)
    {
      char_cnt = (maxwidth/chw)/cos_rad;  //char count in area (average)
    }
  if(char_cnt==0)
    {
      char_cnt=1;
    }

  unsigned int pos=0;
  while(1)
    {
      pos=pos+char_cnt;
      if(text.Length()>pos)
        {
          if(text[pos-1]!=_T('\n') && text[pos+1]!=_T('\n') && text[pos]!=_T('\n'))
            {
              text = text.insert(pos++,_T('\n'));
            }
        }
      else
        {
          return;
        }
    }
}
void RepPrinter::SetFont(Element* elem)
{
  PropertyData* dt = elem->prop.GetProperty(PROP_FONT_ID);
  if(dt==NULL)
    {
      return;
    }

  wxFont* fnt=NULL;

  wxAny data = dt->value.GetAny();
  if(data.CheckType<wxString>())
    {
      wxString ud = data.As<wxString>();
      fnt = new wxFont(ud);
    }
  if(data.CheckType<wxFont>())
    {
      fnt= new wxFont(data.As<wxFont>());
    }
  if(!fnt || !fnt->IsOk())
    {
      fnt = (wxFont*)&curdc_->GetFont();
    }
  curdc_->SetFont(*fnt);
}
// ===============================================

// ===============================================
// Drawing elements
void RepPrinter::DrawLine(Element* elem)
{
  wxPoint lt=GetPoint(elem);
  wxPoint rb=GetPoint(elem,false);

  curdc_->SetPen(*curpen_);
  curdc_->DrawLine( lt, rb );
}
void RepPrinter::DrawRect(Element* elem)
{
#ifdef LOG
  wxString msg = _T("DrawRect: ");
  WRITELOG(msg);
#endif // LOG

  wxPoint lt=GetPoint(elem);
  wxSize sz =GetSize(elem);

#ifdef LOG
  msg.Printf("point(%d,%d) size(%d,%d)\n",lt.x,lt.y,sz.x,sz.y);
  WRITELOG(msg);
#endif // LOG

  wxPoint rb(lt.x+sz.GetWidth(),lt.y+sz.GetHeight());

  wxRect rect;

  rect.SetLeftTop(lt);
  rect.SetRightBottom(rb);

  SetLineSettings(elem);
  SetBrushSettings(elem);

  curdc_->SetPen(*curpen_);
  curdc_->SetBrush(*curbrush_);
  curdc_->DrawRectangle(rect);
}
void RepPrinter::DrawText(Element* elem)
{
  SetFont(elem);
  wxPoint lt = GetPoint(elem);

  SetLineSettings(elem);
  curdc_->SetTextForeground(curpen_->GetColour());

  wxString text(elem->prop.GetProperty(PROP_TEXT_ID)->value.GetString());
  text.Replace(_T("\\n"),_T('\n'));
  GetVariablesValue(text);

  long angle = elem->prop.GetProperty(PROP_TEXTANGLE_ID)->value.GetLong();

  //Text wrapping
  wxCoord ch_height = curdc_->GetCharHeight();
  if(elem->prop.GetProperty(PROP_TEXTWRAP_ID)->value==true)
    {
      long dts = doc_->prop.GetProperty(PROP_SIZEMETER_ID)->value.GetLong();
      long dtl = elem->prop.GetProperty(PROP_POSLEFT_ID)->value.GetLong();
      long dtr = elem->prop.GetProperty(PROP_POSRIGHT_ID)->value.GetLong();
      int mwidth = (dtr - dtl)*dts;

      WrapText(text,mwidth,angle);
    }

  while(1)
    {
      wxString tmp = text.BeforeFirst(_T('\n'));
      if(tmp.IsEmpty())
        {
          tmp=text;
        }
      text = text.AfterFirst(_T('\n'));

      if(tmp.IsEmpty())
        {
          return;
        }
      curdc_->DrawRotatedText(tmp,lt,angle);
      lt.y += ch_height;

    }
}
void RepPrinter::DrawImage(Element* elem)
{
  wxString filename( elem->prop.GetProperty(PROP_IMGFILE_ID)->value.GetString() );
  if(filename==wxEmptyString)
    {
      return;
    }

  wxPoint lt = GetPoint(elem);
  wxSize sz = GetSize(elem);
  if(sz.GetHeight()==0 || sz.GetWidth()==0)
    {
      return;
    }

  wxImage image;
  wxFile fil;
  if(!fil.Exists(filename))
    {
      return;
    }
  if(!image.LoadFile(filename))
    {
      return;
    }

  wxImageResizeQuality qual = wxIMAGE_QUALITY_NEAREST;
  if(visible_flag_==REPORT)
    {
      qual = wxIMAGE_QUALITY_HIGH;
    }
  int width = sz.GetWidth();
  int height = sz.GetHeight();
  image = image.Rescale(abs(width),abs(height), qual);
  if(width<0)
    {
      lt.x += width;
    }
  if(height<0)
    {
      lt.y += height;
    }
  curdc_->DrawBitmap(image,lt,true);
}
// ===============================================
