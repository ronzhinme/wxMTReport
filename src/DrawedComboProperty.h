
#ifndef DRAWEDCOMBOPROPERTY_H
#define DRAWEDCOMBOPROPERTY_H

#include <wx/wx.h>
#include <wx/propgrid/property.h>
#include <wx/propgrid/props.h>

class PenStyleComboProperty : public wxEnumProperty
{
public:
    PenStyleComboProperty( const wxString& label,
                         const wxString& name,
                         wxPGChoices& choices,
                         int value = 0);

    ~PenStyleComboProperty();
    static wxPenStyle GetPenStyle(long indx);
private:
    virtual wxSize OnMeasureImage (int item=-1) const;
    virtual void  OnCustomPaint (wxDC &dc, const wxRect &rect, wxPGPaintData &paintdata);
};

class FillStyleComboProperty : public wxEnumProperty
{
public:
    FillStyleComboProperty( const wxString& label,
                         const wxString& name,
                         wxPGChoices& choices,
                         int value = 0);

    ~FillStyleComboProperty();
    static wxBrushStyle GetBrushStyle(long indx);
private:
    virtual wxSize OnMeasureImage (int item=-1) const;
    virtual void  OnCustomPaint (wxDC &dc, const wxRect &rect, wxPGPaintData &paintdata);
};
#endif // DRAWEDCOMBOPROPERTY_H
