#ifndef VARSDIALOG_H
#define VARSDIALOG_H

//(*Headers(VarsDialog)
#include <wx/sizer.h>
#include <wx/grid.h>
#include <wx/button.h>
#include <wx/dialog.h>
//*)
#include "./ObjModelLib/RepVar.h"
class VarsDialog: public wxDialog
{
	public:

		VarsDialog(wxWindow* parent,wxWindowID id=wxID_ANY,const wxPoint& pos=wxDefaultPosition,const wxSize& size=wxDefaultSize);
		virtual ~VarsDialog();

		//(*Declarations(VarsDialog)
		wxGrid* Grid1;
		wxButton* Btn_Cancel;
		wxButton* Btn_Apply;
		//*)
    void SetVars(wxVector<Var> vars);
    wxVector<Var> GetVars();
	protected:

		//(*Identifiers(VarsDialog)
		static const long ID_GRID1;
		static const long ID_BTNCANCEL;
		static const long ID_BTNAPPLY;
		//*)

	private:

		//(*Handlers(VarsDialog)
		void OnGrid1CellChange(wxGridEvent& event);
		void OnKeyUp(wxKeyEvent& event);
		void OnButtonEndDlgClick(wxCommandEvent& event);
		//*)
		wxVector<Var> vars_;
    bool rowDeleting_;
    void Init();
		DECLARE_EVENT_TABLE()
};

#endif
