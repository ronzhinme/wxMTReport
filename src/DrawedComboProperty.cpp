
#include "DrawedComboProperty.h"

PenStyleComboProperty::PenStyleComboProperty( const wxString& label,
    const wxString& name ,
    wxPGChoices& choices,
    int value)
  : wxEnumProperty(label, name, choices, value)
{

}
PenStyleComboProperty::~PenStyleComboProperty()
{
}
wxSize PenStyleComboProperty::OnMeasureImage (int item) const
{
  if(item<0)
    return wxSize(0,0);
  return wxSize(-1,-1);
}
void  PenStyleComboProperty::OnCustomPaint (wxDC& dc, const wxRect& rect, wxPGPaintData& paintdata)
{
  const wxPen cur_pen = dc.GetPen();

  wxPen pen(cur_pen);
  long add = paintdata.m_choiceItem;
  if(add<0)
  {
    add = m_value.GetLong();
  }
  wxPenStyle style = GetPenStyle(add);
  pen.SetStyle( style );

  if(style > wxPENSTYLE_STIPPLE )
    pen.SetWidth( rect.width );
  else
    pen.SetWidth( 1 );

  dc.SetPen(pen);
  dc.DrawLine(rect.GetBottomLeft(), rect.GetRightTop());
  dc.SetPen(cur_pen);
}

wxPenStyle PenStyleComboProperty::GetPenStyle(long indx)
{
  switch(indx)
  {
  case 0: return wxPENSTYLE_SOLID;
  case 1: return wxPENSTYLE_DOT ;
  case 2: return wxPENSTYLE_LONG_DASH ;
  case 3: return wxPENSTYLE_SHORT_DASH ;
  case 4: return wxPENSTYLE_DOT_DASH ;
  case 5: return wxPENSTYLE_USER_DASH;
  case 6: return wxPENSTYLE_TRANSPARENT;

  case 7: return wxPENSTYLE_STIPPLE  ;

  case 8: return wxPENSTYLE_BDIAGONAL_HATCH  ;
  case 9: return wxPENSTYLE_CROSSDIAG_HATCH  ;
  case 10: return wxPENSTYLE_FDIAGONAL_HATCH  ;
  case 11: return wxPENSTYLE_CROSS_HATCH  ;
  case 12: return wxPENSTYLE_HORIZONTAL_HATCH   ;
  case 13: return wxPENSTYLE_VERTICAL_HATCH   ;
  }
  return wxPENSTYLE_SOLID;
}

FillStyleComboProperty::FillStyleComboProperty( const wxString& label,
    const wxString& name ,
    wxPGChoices& choices,
    int value)
  : wxEnumProperty(label, name, choices, value)
{

}
FillStyleComboProperty::~FillStyleComboProperty()
{
}
wxSize FillStyleComboProperty::OnMeasureImage (int item) const
{
  if(item<0)
    return wxSize(0,0);
  return wxSize(-1,-1);
}
void  FillStyleComboProperty::OnCustomPaint (wxDC& dc, const wxRect& rect, wxPGPaintData& paintdata)
{
  const wxBrush cur_ = dc.GetBrush();

  wxBrush brush(cur_);

  long add = paintdata.m_choiceItem;
  if(add<0)
  {
    add = m_value.GetLong();
  }

  wxBrushStyle style = GetBrushStyle(add);
  brush.SetStyle( style );
  brush.SetColour( *wxBLACK );

  dc.SetBrush(brush);
  dc.DrawRectangle(rect);
  dc.SetBrush(cur_);
}

wxBrushStyle FillStyleComboProperty::GetBrushStyle(long indx)
{
  switch(indx)
  {
  case 0: return wxBRUSHSTYLE_SOLID;
  case 1: return wxBRUSHSTYLE_TRANSPARENT ;
  case 2: return wxBRUSHSTYLE_STIPPLE ;
  case 3: return wxBRUSHSTYLE_BDIAGONAL_HATCH ;
  case 4: return wxBRUSHSTYLE_CROSSDIAG_HATCH ;
  case 5: return wxBRUSHSTYLE_FDIAGONAL_HATCH;
  case 6: return wxBRUSHSTYLE_CROSS_HATCH;
  case 7: return wxBRUSHSTYLE_HORIZONTAL_HATCH  ;
  case 8: return wxBRUSHSTYLE_VERTICAL_HATCH  ;
  }
  return wxBRUSHSTYLE_SOLID;
}
