
#ifndef FILEDRAGTARGET_H
#define FILEDRAGTARGET_H

#include <wx/dnd.h>

class FileDrop:public wxFileDropTarget
{
public:
  FileDrop(wxFrame* Owner);
  ~FileDrop();
  virtual bool OnDropFiles(wxCoord  x,  wxCoord  y,  const wxArrayString &  filenames );
private:
  wxFrame* frm_;
};
#endif // FILEDRAGTARGET_H
