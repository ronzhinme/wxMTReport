
#ifndef MTREPORTLIBH
#define MTREPORTLIBH

#if defined __WXMSW__
extern "C" _declspec(dllexport)  bool             OpenReport(const char* filename);
extern "C" _declspec(dllexport)  long             GetPageCount();
extern "C" _declspec(dllexport)  const char*   GetPageName(long indx);
extern "C" _declspec(dllexport)  bool             SetCurrentDocument(long indx);
extern "C" _declspec(dllexport)  long             GetVarCount();
extern "C" _declspec(dllexport)  const char*   GetVarName(long indx);
extern "C" _declspec(dllexport)  const char*   GetVarVal(long indx);
extern "C" _declspec(dllexport)  bool             SetVarVal(long indx,char* val);
extern "C" _declspec(dllexport)  void             PrintPage();
#else
bool        OpenReport(const char* filename);
long        GetPageCount();
const char* GetPageName(long indx);
bool        SetCurrentDocument(long indx);
long        GetVarCount();
const char* GetVarName(long indx);
const char* GetVarVal(long indx);
bool        SetVarVal(long indx,char* val);
void        PrintPage();
#endif


#endif // MTREPORTLIBH
