#ifndef VERSION_H
#define VERSION_H

namespace AutoVersion{
	
	//Date Version Types
	#define VERS_DATE "21"
	#define VERS_MONTH "12"
	#define VERS_YEAR "2016"
	#define VERS_UBUNTU_VERSION_STYLE  "16.12"
	
	//Software Status
	#define VERS_STATUS  ""
	#define VERS_STATUS_SHORT  ""
	
	//Standard Version Type
	#define VERS_MAJOR  0
	#define VERS_MINOR  0
	#define VERS_BUILD  3
	#define VERS_REVISION  14
	
	//Miscellaneous Version Types
	#define VERS_BUILDS_COUNT  12
	#define VERS_RC_FILEVERSION 0,0,3,14
	#define VERS_RC_FILEVERSION_STRING "0, 0, 3, 14\0"
	#define VERS_FULLVERSION_STRING  "0.0.3.14"
	
	//These values are to keep track of your versioning state, don't modify them.
	#define VERS_BUILD_HISTORY  3
	

}
#endif //VERSION_H
