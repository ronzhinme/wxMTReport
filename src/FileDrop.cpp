
#include "FileDrop.h"
#include "wxMTReportMain.h"

FileDrop::FileDrop(wxFrame* Owner)
{
  frm_ = Owner;
}
FileDrop::~FileDrop()
{
}
bool FileDrop::OnDropFiles(wxCoord  x,  wxCoord  y,  const wxArrayString&   filenames )
{
  if(filenames.GetCount () ==0)
  {
    return false;
  }
  wxMTReportFrame* frm = reinterpret_cast<wxMTReportFrame*>(frm_);
  if(!frm)return false;

  frm->LoadProject(filenames[0]);
  return true;
}
