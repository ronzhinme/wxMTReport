#ifndef ABOUTDIALOG_H
#define ABOUTDIALOG_H

//(*Headers(AboutDialog)
#include <wx/sizer.h>
#include <wx/stattext.h>
#include <wx/hyperlink.h>
#include <wx/statbmp.h>
#include <wx/button.h>
#include <wx/dialog.h>
//*)

class AboutDialog: public wxDialog
{
	public:

		AboutDialog(wxWindow* parent,wxWindowID id=wxID_ANY);
		virtual ~AboutDialog();

		//(*Declarations(AboutDialog)
		wxButton* Button1;
		wxStaticBitmap* StaticBitmap1;
		wxStaticText* lblVersion;
		wxStaticText* lblAuthor;
		wxHyperlinkCtrl* HyperlinkCtrl1;
		//*)

	protected:

		//(*Identifiers(AboutDialog)
		static const long ID_STATICBITMAP1;
		static const long ID_STATICTEXT1;
		static const long ID_STATICTEXT2;
		static const long ID_HYPERLINKCTRL1;
		static const long ID_BUTTON1;
		//*)

	private:

		//(*Handlers(AboutDialog)
		void OnButtonOKClick(wxCommandEvent& event);
		//*)
    void SetAuthor();
    void SetVersion();

		DECLARE_EVENT_TABLE()
};

#endif
