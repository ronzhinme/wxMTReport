
#include "./ObjModelLib/defines.h"
#include "version_generator.h"
#include "AboutDialog.h"
#include "wxMTReportApp.h"

#define LOGO_BITMAP_PATH _T("/img/lightReport.png")
//(*InternalHeaders(AboutDialog)
#include <wx/intl.h>
#include <wx/string.h>
//*)

//(*IdInit(AboutDialog)
const long AboutDialog::ID_STATICBITMAP1 = wxNewId();
const long AboutDialog::ID_STATICTEXT1 = wxNewId();
const long AboutDialog::ID_STATICTEXT2 = wxNewId();
const long AboutDialog::ID_HYPERLINKCTRL1 = wxNewId();
const long AboutDialog::ID_BUTTON1 = wxNewId();
//*)

BEGIN_EVENT_TABLE(AboutDialog,wxDialog)
  //(*EventTable(AboutDialog)
  //*)
END_EVENT_TABLE()

AboutDialog::AboutDialog(wxWindow* parent,wxWindowID id)
{
  //(*Initialize(AboutDialog)
  wxBoxSizer* BoxSizer2;
  wxBoxSizer* BoxSizer1;
  wxBoxSizer* BoxSizer3;

  Create(parent, id, _("About"), wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE, _T("id"));
  SetClientSize(wxSize(300,200));
  BoxSizer1 = new wxBoxSizer(wxVERTICAL);
  BoxSizer2 = new wxBoxSizer(wxHORIZONTAL);
  StaticBitmap1 = new wxStaticBitmap(this, ID_STATICBITMAP1, wxNullBitmap, wxDefaultPosition, wxDefaultSize, wxSIMPLE_BORDER, _T("ID_STATICBITMAP1"));
  BoxSizer2->Add(StaticBitmap1, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
  BoxSizer3 = new wxBoxSizer(wxVERTICAL);
  lblAuthor = new wxStaticText(this, ID_STATICTEXT1, _("Author"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT1"));
  BoxSizer3->Add(lblAuthor, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
  lblVersion = new wxStaticText(this, ID_STATICTEXT2, _("Version"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT2"));
  BoxSizer3->Add(lblVersion, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
  HyperlinkCtrl1 = new wxHyperlinkCtrl(this, ID_HYPERLINKCTRL1, _("Send e-mail"), _("mailto:lreprme@gmail.com"), wxDefaultPosition, wxDefaultSize, wxHL_CONTEXTMENU|wxHL_ALIGN_CENTRE|wxNO_BORDER, _T("ID_HYPERLINKCTRL1"));
  BoxSizer3->Add(HyperlinkCtrl1, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
  BoxSizer2->Add(BoxSizer3, 0, wxALL|wxEXPAND|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
  BoxSizer1->Add(BoxSizer2, 1, wxALL|wxEXPAND|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
  Button1 = new wxButton(this, ID_BUTTON1, _("OK"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON1"));
  BoxSizer1->Add(Button1, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
  SetSizer(BoxSizer1);
  SetSizer(BoxSizer1);
  Layout();
  Center();

  Connect(ID_BUTTON1,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&AboutDialog::OnButtonOKClick);
  //*)

  StaticBitmap1->SetBitmap(wxBitmap(wxGetApp().AppDir + LOGO_BITMAP_PATH, wxBITMAP_TYPE_PNG));
  SetAuthor();
  SetVersion();
}

AboutDialog::~AboutDialog()
{
  //(*Destroy(AboutDialog)
  //*)
}

void AboutDialog::SetAuthor()
{
  lblAuthor->SetLabel( _("Author")+_T(": ") + AUTHOR);
}
void AboutDialog::SetVersion()
{
  lblVersion->SetLabel(_("Version")+_T(": ") + wxString(VERS_FULLVERSION_STRING)+_T("\n")+_("Date")+_T(": ")+wxString(VERS_DATE)+_T(".")+wxString(VERS_MONTH)+_T(".")+wxString(VERS_YEAR));
}

void AboutDialog::OnButtonOKClick(wxCommandEvent& event)
{
  EndModal(wxID_OK);
}
