
#include "wxMTReportApp.h"
#include "SettingsDialog.h"


//#include <wx/propgrid/property.h>

//(*InternalHeaders(SettingsDialog)
#include <wx/intl.h>
#include <wx/string.h>
//*)

//(*IdInit(SettingsDialog)
const long SettingsDialog::ID_STATICTEXT1 = wxNewId();
const long SettingsDialog::ID_COMBOBOX1 = wxNewId();
const long SettingsDialog::ID_BUTTON1 = wxNewId();
const long SettingsDialog::ID_BUTTON2 = wxNewId();
//*)

BEGIN_EVENT_TABLE(SettingsDialog,wxDialog)
  //(*EventTable(SettingsDialog)
  //*)
END_EVENT_TABLE()

SettingsDialog::SettingsDialog(wxWindow* parent)
{
  //(*Initialize(SettingsDialog)
  wxBoxSizer* BoxSizer2;
  wxBoxSizer* BoxSizer1;
  wxBoxSizer* BoxSizer3;

  Create(parent, wxID_ANY, _("Settings"), wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE, _T("wxID_ANY"));
  BoxSizer1 = new wxBoxSizer(wxVERTICAL);
  BoxSizer2 = new wxBoxSizer(wxHORIZONTAL);
  StaticText1 = new wxStaticText(this, ID_STATICTEXT1, _("Language"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT1"));
  BoxSizer2->Add(StaticText1, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
  cb_language = new wxComboBox(this, ID_COMBOBOX1, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, 0, 0, wxDefaultValidator, _T("ID_COMBOBOX1"));
  BoxSizer2->Add(cb_language, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
  BoxSizer1->Add(BoxSizer2, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
  BoxSizer3 = new wxBoxSizer(wxHORIZONTAL);
  Button1 = new wxButton(this, ID_BUTTON1, _("Cancel"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON1"));
  BoxSizer3->Add(Button1, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
  Button2 = new wxButton(this, ID_BUTTON2, _("Apply"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON2"));
  BoxSizer3->Add(Button2, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
  BoxSizer1->Add(BoxSizer3, 1, wxALL|wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL, 5);
  SetSizer(BoxSizer1);
  BoxSizer1->Fit(this);
  BoxSizer1->SetSizeHints(this);
  Center();

  Connect(ID_COMBOBOX1,wxEVT_COMMAND_COMBOBOX_SELECTED,(wxObjectEventFunction)&SettingsDialog::Oncb_languageSelected);
  Connect(ID_BUTTON1,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&SettingsDialog::OnButtonCancel_Click);
  Connect(ID_BUTTON2,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&SettingsDialog::OnButtonApplyClick);
  //*)
  LanguageSettingInit();
}

SettingsDialog::~SettingsDialog()
{
  //(*Destroy(SettingsDialog)
  //*)
}

void SettingsDialog::LanguageSettingInit()
{
  lang_arr.Clear();
  cb_language->Clear();
  lang_arr = wxGetApp().GetAvailableLanguageChoises()->Copy();
  cb_language->Append(lang_arr.GetLabels());
  cb_language->SetSelection(lang_arr.Index(wxGetApp().cnf->inf.lang_num));
}


void SettingsDialog::Oncb_languageSelected(wxCommandEvent& event)
{

}

void SettingsDialog::OnButtonCancel_Click(wxCommandEvent& event)
{
  EndModal(wxID_CANCEL);
}

void SettingsDialog::OnButtonApplyClick(wxCommandEvent& event)
{
  wxGetApp().cnf->inf.lang_num=lang_arr.GetValue(cb_language->GetSelection());
  wxGetApp().SelectLanguage(wxGetApp().cnf->inf.lang_num);
  EndModal(wxID_APPLY);
}
