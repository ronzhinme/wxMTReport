
#include "RepVar.h"

Var* RepVars::FindVar(wxString varname)
{
  if(vars_.empty()) { return NULL; }
  wxVector<Var>::iterator iter;
  for(iter=vars_.begin(); iter!=vars_.end(); iter++)
  {
    if((*iter).name.IsSameAs(varname,true))
    {
      return iter;
    }
  }
  return NULL;
}
bool RepVars::AddVar(wxString varname)
{
  if(FindVar(varname)!=NULL)
  {
    return false;
  }
  Var v(varname);
  vars_.push_back(v);
  return true;
}
bool RepVars::DelVar(wxString varname)
{
  Var* v = FindVar(varname);
  if(v==NULL)
  {
    return false;
  }

  vars_.erase(v);
  return true;
}
void RepVars::Clear()
{
  vars_.clear();
}
wxVector<Var> RepVars::GetVars()
{
  return vars_;
}
int RepVars::SetVars(wxString s)
{
  Clear();
  s.Replace(_T("\\n"),_T("\n"));
  while(s.Contains(_T("\n\n")))
  {
    s.Replace(_T("\n\n"),_T("\n"));
  }

  wxString v = s.BeforeFirst(_T('\n'));
  while(!v.IsEmpty())
  {
    s.Replace(v,wxEmptyString,false);
    s.Replace(_T('\n'),wxEmptyString,false);
    AddVar(v);
    v=s.BeforeFirst(_T('\n'));
  }
  return vars_.size();
}
int RepVars::SetVars(wxVector<Var> s)
{
  vars_.clear();
  vars_.assign(s.begin(), s.end());
  return vars_.size();
}
wxString RepVars::ToString()
{
  wxString s;
  for(wxVector<Var>::iterator iter = vars_.begin(); iter!=vars_.end();++iter)
  {
    s.Printf("%s%s\\n",s,iter->value);
  }
  return s;
}
