
#ifndef ELEMENT_H
#define ELEMENT_H

#include <wx/vector.h>
#include <wx/gdicmn.h>
#include <wx/propgrid/manager.h>
#include <wx/treectrl.h>

#include "PropMngr.h"
#include "defines.h"
#include "Utils.h"
namespace ElementModel
{

class RepTreeData:public wxTreeItemData
{
public:
  eRepElementType type;
  long id;
};

class Element
{
private:
  void Init(wxString name,eRepElementType typ);
public:
  RepProperties prop;
  long id;

  Element();
  Element(wxString name);
  Element(wxString name, eRepElementType typ);
  Element(const Element& elbas);

  eRepElementType GetType();
  wxString GetName();

  void SetType(eRepElementType typ);
  void SetName(wxString name);

  void GetIDFromString(const wxString file_data);
  wxString IDToString();
  void FromString(const wxString file_data);
  wxString ToString();

  void ChangeSizeScale(double kf);
};
}
#endif // ELEMENT_H

