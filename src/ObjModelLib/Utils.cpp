
#include "Utils.h"

wxString GetSection(wxString text, wxString tag_beg, wxString tag_end)
{
  int find_posbeg = text.Find(tag_beg);
  int find_posend = text.Find(tag_end);
  if(find_posend==wxNOT_FOUND || find_posbeg==wxNOT_FOUND)
  {
    return wxEmptyString;
  }

  return text.SubString(find_posbeg,find_posend+(tag_end.Len()-1));
}

wxString GetSectionValue(wxString text, wxString tag_beg, wxString tag_end)
{
  wxString sect = GetSection(text,tag_beg,tag_end);
  sect.Replace(tag_beg,_T(""));
  sect.Replace(tag_end,_T(""));
  return sect;
}
