
#include "PropMngr.h"
using namespace ElementModel;

void PropertyManager::PrepareElementProperties(eRepElementType typ, RepProperties& props)
{
  CleanProperties(props);
  switch(typ)
  {
  case EL_LINE:
    LinePropertiesPrepare(props);
    break;
  case EL_TEXT:
    TextPropertiesPrepare(props);
    break;
  case EL_IMAGE:
    ImagePropertiesPrepare(props);
    break;
  case EL_RECT:
    RectPropertiesPrepare(props);
    break;
  case EL_PAGE:
    PagePropertiesPrepare(props);
    break;
  case EL_UNKNOWN:
  case EL_NULL:
    break;
  }
}
void PropertyManager::CleanProperties(RepProperties& props)
{
  long cnt = props.GetCount();
  if(cnt>2)
  {
    props.DelProperty(2,cnt);
  }
  VisibilityPropertiesPrepare(props);
}

//=======================================================================
// ���������� ������� ��� ����������� ���� ��������

void PropertyManager::LinePropertiesPrepare(RepProperties& props)
{
  PointPropertiesPrepare(props);
  PointPropertiesPrepare(props,false);
  PenPropertiesPrepare(props);
  LayerPropertiesPrepare(props);
}
void PropertyManager::TextPropertiesPrepare(RepProperties& props)
{
  PointPropertiesPrepare(props);
  PointPropertiesPrepare(props,false);
  FontPropertiesPrepare(props);
  TextColorPropertiesPrepare(props);
  LayerPropertiesPrepare(props);

  PropertyData pd(PROP_TEXT_ID,_T(""),PR_STRING, PCAT_COMMON_ID);
  PropertyData pd1(PROP_TEXTWRAP_ID,false,PR_BOOL, PCAT_COMMON_ID);
  PropertyData pd2(PROP_TEXTANGLE_ID,0,PR_FLOAT, PCAT_COMMON_ID);
  props.AddProperty(pd);
  props.AddProperty(pd1);
  props.AddProperty(pd2);
}
void PropertyManager::ImagePropertiesPrepare(RepProperties& props)
{
  PointPropertiesPrepare(props);
  SizePropertiesPrepare(props);
  ImgFileSelectorPrepare(props);
  LayerPropertiesPrepare(props);
}
void PropertyManager::RectPropertiesPrepare(RepProperties& props)
{
  PointPropertiesPrepare(props);
  SizePropertiesPrepare(props);
  PenPropertiesPrepare(props);
  BrushPropertiesPrepare(props);
  LayerPropertiesPrepare(props);
}
void PropertyManager::PagePropertiesPrepare(RepProperties& props)
{
  PaperSizePropertyPrepare(props);
  SizeScalePropertiesPrepare(props);
  SizePropertiesPrepare(props,false);

  PropertyData pd1(PAPER_BGROUND_ID, DEFAULT_BGROUND_COLOUR,PR_COLOR,PCAT_COMMON_ID);
  props.AddProperty(pd1);
}
//=======================================================================

//=======================================================================
// ���������� ������� ���������
void PropertyManager::SizeScalePropertiesPrepare(RepProperties& props)
{
  PropertyData pd(PROP_SIZEMETER_ID,1,PR_SWITCH,PCAT_SIZE_ID);
  pd.choise_id = SIZESCALE_LABEL_ID;
  props.AddProperty(pd);
}
void PropertyManager::VisibilityPropertiesPrepare(RepProperties& props)
{
  PropertyData pd(PROP_VISIBLE_ID,3,PR_BOOLARRAY,PCAT_COMMON_ID);
  pd.choise_id = VISIBLE_METHOD_ID;
  props.AddProperty(pd);
}
void PropertyManager::PointPropertiesPrepare(RepProperties& props,bool is_lefttop)
{
  PropertyData* pd,*pd1;
  if(is_lefttop)
  {
    pd=new PropertyData(PROP_POSTOP_ID,0,PR_FLOAT,PCAT_POSITION_ID);
    pd1=new PropertyData(PROP_POSLEFT_ID,0,PR_FLOAT,PCAT_POSITION_ID);
  }
  else
  {
    pd=new PropertyData(PROP_POSBOTTOM_ID,0,PR_FLOAT,PCAT_POSITION_ID);
    pd1=new PropertyData(PROP_POSRIGHT_ID,0,PR_FLOAT,PCAT_POSITION_ID);
  }
  props.AddProperty(*pd);
  props.AddProperty(*pd1);

  delete pd;
  delete pd1;
}
void PropertyManager::SizePropertiesPrepare(RepProperties& props, bool is_enabled)
{
  PropertyData pd(PROP_HEIGHT_ID,0,PR_FLOAT,PCAT_SIZE_ID);
  PropertyData pd1(PROP_WIDTH_ID,0,PR_FLOAT,PCAT_SIZE_ID);
  if(!is_enabled)
  {
  pd.is_enabled=false;
  pd1.is_enabled=false;
  }
  props.AddProperty(pd);
  props.AddProperty(pd1);
}
void PropertyManager::PaperSizePropertyPrepare(RepProperties& props)
{
  PropertyData pd(PROP_PAGESIZE_ID,DEFAULT_PAPERSIZE,PR_SWITCH,PCAT_SIZE_ID);
  pd.choise_id=PAPERSIZE_LABEL_ID;
  props.AddProperty(pd);

  PropertyData pd1(PROP_PAGEORIENT_ID,DEFAULT_PAPERORIENT,PR_SWITCH,PCAT_SIZE_ID);
  pd1.choise_id=PAPERORIENT_LABEL_ID;
  props.AddProperty(pd1);
}
void PropertyManager::FontPropertiesPrepare(RepProperties& props)
{
  wxFont fnt=wxSystemSettings::GetFont(wxSYS_ANSI_FIXED_FONT);
  PropertyData pd (PROP_FONT_ID,fnt.GetNativeFontInfoDesc() ,PR_FONTSELECTOR,PCAT_FONT_ID);
  props.AddProperty(pd);
}
void PropertyManager::BrushPropertiesPrepare(RepProperties& props)
{
  PropertyData pd (PROP_FILLTYP_ID,0,PR_BRUSHTYPE,PCAT_FILL_ID);
  pd.choise_id=BRUSHTYPE_LABEL_ID;

  PropertyData pd1(PROP_FILLCLR_ID,wxBLACK,PR_COLOR,PCAT_FILL_ID);

  props.AddProperty(pd);
  props.AddProperty(pd1);
}
void PropertyManager::PenPropertiesPrepare(RepProperties& props)
{
  PropertyData pd (PROP_PENTYP_ID,0,PR_LINETYPE,PCAT_PEN_ID);
  pd.choise_id=PENTYPE_LABEL_ID;

  PropertyData pd1(PROP_PENCLR_ID,wxBLACK,PR_COLOR,PCAT_PEN_ID);
  PropertyData pd2(PROP_PENWEIGHT_ID,1,PR_INTEGER,PCAT_PEN_ID);

  props.AddProperty(pd);
  props.AddProperty(pd1);
  props.AddProperty(pd2);
}
void PropertyManager::TextColorPropertiesPrepare(RepProperties& props)
{
  PropertyData pd1(PROP_PENCLR_ID,wxBLACK,PR_COLOR,PCAT_FONT_ID);
  props.AddProperty(pd1);
}
void PropertyManager::FileSelectorPrepare(RepProperties& props)
{
  PropertyData pd (PROP_FILE_ID,wxEmptyString,PR_FILESELECTOR,PCAT_FILE_ID);
  props.AddProperty(pd);
}
void PropertyManager::ImgFileSelectorPrepare(RepProperties& props)
{
  PropertyData pd (PROP_IMGFILE_ID,wxEmptyString,PR_IMGFILESELECTOR,PCAT_FILE_ID);
  props.AddProperty(pd);
}
void PropertyManager::LayerPropertiesPrepare(RepProperties& props)
{
  PropertyData pd (PROP_LAYER_ID,0,PR_INTEGER,PCAT_COMMON_ID);
  props.AddProperty(pd);
}
//=======================================================================


