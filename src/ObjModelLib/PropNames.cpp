#include "PropNames.h"

void PropertyNames::Refresh( wxString* prop_names)
{
  prop_names[PROP_UNKNOWN_ITEM_ID]=PROP_UNKNOWN_ITEM;
  prop_names[PROP_UNKNOWN_PAGE_ID]=PROP_UNKNOWN_PAGE;
  prop_names[PROP_NAME_ID]=PROP_NAME;
  prop_names[PROP_TYPE_ID]=PROP_TYPE;
  prop_names[PROP_POSTOP_ID]=PROP_POSTOP;
  prop_names[PROP_POSLEFT_ID]=PROP_POSLEFT;
  prop_names[PROP_POSBOTTOM_ID]=PROP_POSBOTTOM;
  prop_names[PROP_POSRIGHT_ID]=PROP_POSRIGHT;
  prop_names[PROP_FILLTYP_ID]=PROP_FILLTYP;
  prop_names[PROP_FILLCLR_ID]=PROP_FILLCLR;
  prop_names[PROP_HEIGHT_ID]=PROP_HEIGHT;
  prop_names[PROP_WIDTH_ID]=PROP_WIDTH;
  prop_names[PROP_FONT_ID]=PROP_FONT;
  prop_names[PROP_PENTYP_ID]=PROP_PENTYP;
  prop_names[PROP_PENCLR_ID]=PROP_PENCLR;
  prop_names[PROP_PENWEIGHT_ID]=PROP_PENWEIGHT;
  prop_names[PROP_FILE_ID]=PROP_FILE;
  prop_names[PROP_IMGFILE_ID]=PROP_IMGFILE;
  prop_names[PROP_TEXT_ID]=PROP_TEXT;
  prop_names[PROP_TEXTWRAP_ID]=PROP_TEXTWRAP;
  prop_names[PROP_TEXTANGLE_ID]=PROP_TEXTANGLE;

  prop_names[PROP_VISIBLE_ID]=PROP_VISIBLE;
  prop_names[PROP_SIZEMETER_ID]=PROP_SIZEMETER;
  prop_names[PROP_PAGESIZE_ID]=PROP_PAGESIZE;
  prop_names[PROP_PAGEORIENT_ID]=PROP_PAGEORIENT;
  prop_names[PROP_VARSLIST_ID]=PROP_VARSLIST;
  prop_names[PROP_LAYER_ID]=PROP_LAYER;

  prop_names[PGPAGE_PROP_ID]=PGPAGE_PROP;
  prop_names[PGPAGE_VARS_ID]=PGPAGE_VARS;

  prop_names[PCAT_COMMON_ID]=PCAT_COMMON;
  prop_names[PCAT_POSITION_ID]=PCAT_POSITION;
  prop_names[PCAT_FILL_ID]=PCAT_FILL;
  prop_names[PCAT_SIZE_ID]=PCAT_SIZE;
  prop_names[PCAT_FONT_ID]=PCAT_FONT;
  prop_names[PCAT_PEN_ID]=PCAT_PEN;
  prop_names[PCAT_FILE_ID]=PCAT_FILE;

  prop_names[VISIBLE_METHOD_ID]=VISIBLE_METHOD;
  prop_names[REPORT_ELEMENT_ID]=REPORT_ELEMENT;
  prop_names[SIZESCALE_LABEL_ID]=SIZESCALE_LABEL;
  prop_names[SIZESCALE_VALUE_ID]=SIZESCALE_VALUE;

  prop_names[PAPERSIZE_LABEL_ID]=PAPERSIZE_LABEL;
  prop_names[PAPERSIZE_VALUE_ID]=PAPERSIZE_VALUE;
  prop_names[PAPERORIENT_LABEL_ID]=PAPERORIENT_LABEL;
  prop_names[PAPERORIENT_VALUE_ID]=PAPERORIENT_VALUE;
  prop_names[PAPER_BGROUND_ID]=PROP_BGROUND;
  prop_names[PENTYPE_LABEL_ID]=PENTYPE_LABEL;
  prop_names[BRUSHTYPE_LABEL_ID]=BRUSHTYPE_LABEL;
  // TODO этот кусок кода часто вызывается повторно прям раз за разом. нужно оптимизировать, чтоб вызывался только при изменении языка
}
long PropertyNames::GetIDByName(wxString name)
{
  wxString prop_names[MAX_ID];
  Refresh(prop_names);

  for(int f=PROP_UNKNOWN_ITEM_ID; f<MAX_ID; ++f)
  {
    if(prop_names[f].IsSameAs(name))
    {
      return f;
    }
  }
  return -1;
}
wxString PropertyNames::GetNameByID(long id)
{
  if(id<0 || id>=MAX_ID) { return wxEmptyString; }
  wxString prop_names[MAX_ID];
  Refresh(prop_names);
  return prop_names[id];
}

