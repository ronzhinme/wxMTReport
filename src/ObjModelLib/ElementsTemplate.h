#ifndef ELEMENTSTEMPLATE_H
#define ELEMENTSTEMPLATE_H

#include "Element.h"
namespace ElementModel
{
template<class Type> class Elements
{
private:
  wxVector<Type> elems_;
public:
  Elements() {}
  ~Elements() {}

  Type* AddNew(eRepElementType typ=EL_UNKNOWN);
  void Add(Type el);
  void AddRange(Elements<Type> elems);
  bool Delete(wxAny x,bool is_index=true);
  Type* Get(wxAny x, bool is_index=true);
  bool Move(Type* el, long index);
  void Insert(Type el, long index);
  long GetCount();
  long GetIndex(wxAny x);
  void Clear();
  wxString ToString();
  void FromString(const wxString from_filedata);
};


//=======================================================================
// ������ � ������� ������
template <class Type >  bool Elements< Type >::Move(Type* el, long index)
{
  if(index<0 || index>GetCount())
  {
    return false;
  }

  wxString old_name(el->GetName());
  wxString new_name = old_name+_T("!");

  long cur_indx = GetIndex(old_name);
  if(cur_indx>index)
  {
    Insert(*el,index);
    el = Get(index);
  }
  else
  {
    Insert(*el,index+1);
    el = Get(index+1);
  }

  if(el == NULL)
  {
    return false;
  }
  el->SetName(new_name);

  Delete(old_name,false);
  el=Get(new_name,false);
  el->SetName(old_name);

  return true;
}
template <class Type > void Elements< Type >::Add(Type el)
{
  elems_.push_back(el);
}
template <class Type > Type* Elements< Type >::AddNew(eRepElementType typ)
{
  long postfix =0;
  int base_id = PROP_UNKNOWN_ITEM_ID;
  if(typ==EL_PAGE)
  {
    base_id = PROP_UNKNOWN_PAGE_ID;
  }


  while( Get(PropertyNames::GetNameByID(base_id)+wxString::FromDouble(postfix),false)!=NULL )
  {
    ++postfix;
  }
  Type el(PropertyNames::GetNameByID(base_id)+wxString::FromDouble(postfix),typ);
  Add(el);
  return &elems_.back();
}
template <class Type > bool  Elements< Type >::Delete(wxAny x, bool is_index)
{
  if(!is_index)
  {
    x= GetIndex(x);
    if(x.As<long>()==-1)
    {
      return false;
    }
  }

  if(x.CheckType<long>() && x.As<long>() < (long)elems_.size() && x.As<long>()>=0 )
  {
    elems_.erase(elems_.begin()+x.As<long>());
    return true;
  }
  return false;
}
template <class Type > Type* Elements< Type >::Get(wxAny x, bool is_index)
{
  if(!is_index)
  {
    x=GetIndex(x);
    if(x==-1)
    {
      return NULL;
    }
  }

  if(x.CheckType<long>()==true)
  {
    long indx = x.As<long>();
    if(indx>=GetCount() || indx<0)
    {
      return NULL;
    }
    return &(elems_.at(indx));
  }
  return NULL;
}
template <class Type > long  Elements< Type >::GetIndex(wxAny x)
{
  for(long f=0; f<GetCount(); ++f)
  {
    Type* el = Get(f);
    if(el==NULL)
    {
      continue;
    }

    if(x.CheckType<wxString>()==true)
    {
      wxString name = x.As<wxString>();
      if(el->GetName().IsSameAs(name))
      {
        return f;
      }
    }
    if(x.CheckType<long>()==true)
    {
      long tmp =x.As<long>();
      if(el->id==tmp)
      {
        return f;
      }
    }
  }
  return -1;
}
template <class Type > long  Elements< Type >::GetCount()
{
  return elems_.size();
}
template <class Type > wxString  Elements< Type >::ToString()
{
  wxString res=wxEmptyString;
  for(int f=0; f<GetCount(); f++)
  {
    res+=elems_.at(f).ToString();
  }
  return res;
}
template <class Type > void  Elements< Type >::FromString(const wxString from_filedata)
{
  wxString data=from_filedata;

  wxString beg_tag=DOCESECT_START;
  wxString end_tag=DOCESECT_STOP;
  Type elem;
  if(elem.GetType()==EL_PAGE)
  {
    beg_tag = DOCSECT_START;
    end_tag = DOCSECT_STOP;
  }

  wxString tmp = GetSectionValue(data,beg_tag,end_tag);
  wxString delstr = GetSection(data,beg_tag,end_tag);
  if(delstr!=wxEmptyString)
  {
    data.Replace(delstr,wxEmptyString);
  }

  while(tmp!=wxEmptyString)
  {
    Type element;
    element.FromString(tmp);
    Add(element);
#ifdef LOG
    wxString msg;
    msg.Printf("Load Add: type=%d name=%s id=%d\n",element.GetType(),element.GetName(),element.id);
    WRITELOG(msg);
#endif // LOG
    tmp = GetSectionValue(data,beg_tag,end_tag);
    delstr = GetSection(data,beg_tag,end_tag);
    if(delstr!=wxEmptyString)
    {
      data.Replace(delstr,wxEmptyString);
    }
  }
}
template <class Type > void Elements< Type >::Clear()
{
  elems_.clear();
  elems_.swap(elems_);
}
template <class Type > void Elements< Type >::AddRange(Elements<Type> elems)
{
  for(long f=0; f<elems.GetCount(); ++f)
  {
    elems_.push_back( *(elems.Get(f)) );
  }
}
template <class Type > void Elements< Type >::Insert(Type el, long index)
{
  elems_.insert(elems_.begin()+index,el);
}
//=======================================================================

}
#endif // ELEMENTSTEMPLATE_H

