
#ifndef PROPNAMES_H
#define PROPNAMES_H

#include <wx/wx.h>
#include "defines.h"

class PropertyNames
{
private:

  static void Refresh(  wxString* prop_names);
public:
  static long GetIDByName(wxString name);
  static wxString GetNameByID(long id);
};

#endif // PROPNAMES_H
