
#include <wx/paper.h>
#include "Document.h"

#ifdef LOG
#include <wx/log.h>
#endif // LOG

namespace ElementModel
{
void Doc::Init()
{
  SetType(EL_PAGE);
  ChangePaperSize(DEFAULT_PAPERSIZE);
  ChangePaperOrient(DEFAULT_PAPERORIENT);
}
//! \brief
//! Инициализация элемента типа Страница
//! \param
//! \param
//! \return
//!
//!
Doc::Doc()
{
  Init();
}
Doc::Doc(wxString name):Element(name)
{
  Init();
}
Doc::Doc(wxString name, eRepElementType typ):Element(name)
{
  Init();
}
Doc::Doc(const Element& elbas)
{
  Init();
}
Doc::~Doc()
{
}
void Doc::ChangePaperOrient(long orient_id)
{
  if(orient_id == (long)print_dt.GetOrientation()) { return; }

  int w,h;
  w=print_dt.GetPaperSize().GetWidth();
  h=print_dt.GetPaperSize().GetHeight();

  wxSize sizemm;
  if((w<h && orient_id==(int)wxLANDSCAPE) || (w>h && orient_id==(int)wxPORTRAIT))
  {
    sizemm.Set(h,w);
  }
  else
    sizemm.Set(w,h);

  wxPaperSize sz = wxThePrintPaperDatabase->GetSize(sizemm);
  if(sz)
  {
    print_dt.SetOrientation( (wxPrintOrientation)orient_id );
    ChangePaperSize(sz);
  }
}

bool Doc::GetSizeScaleKoef(double& koef, double& pkoef)
{
  PropertyData* pd = prop.GetProperty(PROP_SIZEMETER_ID);
  if(pd==NULL)
  {
    return false;
  }
  long sizescale = pd->value.GetLong();
  koef=1;
  pkoef=1;
  switch(sizescale)
  {
  case 1:
    pkoef=0.1;
    break;
  case 10:
    koef=0.1;
    break;
  case 100:
    koef = 0.01;
    pkoef=10;
    break;
  }
  return true;
}

void Doc::ChangePaperSize(long paper_id)
{
  double koef;
  double pkoef;
  if(!GetSizeScaleKoef(koef,pkoef)) { return; }

  print_dt.SetPaperId( (wxPaperSize)paper_id );
  print_dt.SetPaperSize(wxThePrintPaperDatabase->GetSize((wxPaperSize)paper_id));

  //размер в их измерениях
  prop.GetProperty(PROP_WIDTH_ID)->value = wxVariant(print_dt.GetPaperSize().GetWidth()*koef);
  prop.GetProperty(PROP_HEIGHT_ID)->value = wxVariant(print_dt.GetPaperSize().GetHeight()*koef);
}

void Doc::ChangeSizeScale(long scale_val)
{
  long old_scale_val=scale_val;
  PropertyData* pd = prop.GetProperty(PROP_SIZEMETER_ID);
  if(pd==NULL)
  {
    return;
  }

  old_scale_val = pd->value.GetLong();
  if(old_scale_val==scale_val)
  {
    return;
  }

  double koef = (double)old_scale_val/(double)scale_val;

  //пересчет размеров страницы с учетом коэфициента
  pd = prop.GetProperty(PROP_WIDTH_ID);
  pd->value = pd->value.GetDouble()*koef;
  pd = prop.GetProperty(PROP_HEIGHT_ID);
  pd->value = pd->value.GetDouble()*koef;

  //изменить размеры элементов на странице
  for(int f=0; f<elems.GetCount(); ++f)
  {
    elems.Get(f,true)->ChangeSizeScale(koef);
  }
}
void Doc::FromString(const wxString file_data)
{
  GetIDFromString(file_data);
  prop.FromString(file_data);
  elems.Clear();
  elems.FromString(file_data);
  ChangePaperSize(prop.GetProperty(PROP_PAGESIZE_ID)->value.GetLong() );
  ChangePaperOrient( prop.GetProperty(PROP_PAGEORIENT_ID)->value.GetLong() );
}
wxString Doc::ToString()
{
  wxString Idstr = IDToString();
  return  DOCSECT_START+ Idstr + prop.ToString() + elems.ToString() + DOCSECT_STOP;
}

}

