
#ifndef REPPROJECT_H
#define REPPROJECT_H

#include <wx/vector.h>
#include <wx/ffile.h>

#include <zlib.h>

#include "Document.h"

#include "RepVar.h"
#include "defines.h"
namespace ElementModel
{

class RepProj
{
private:
  bool CompressData(wxString data, wxString filename);
  bool DecompressData(wxString &data, wxString filename);
  bool CompressFile(wxString filename,bool is_compress, wxString& filedata);

  void SaveSel(wxString& filedata);
  void LoadSel(wxString& filedata);
  void SaveVars(wxString& filedata);
  void LoadVars(wxString& filedata);
public:
  int selected_item;
  Elements<Doc> docs;
  RepVars vars;
  RepProperties prop;
  wxString ProjFile;
  bool IsModify;

  RepProj();

  void Clear();
  bool LoadProject(wxString filename=wxEmptyString);
  bool SaveProject(wxString filename=wxEmptyString);

  void SetSelected(int id, bool is_page);
  int GetSelected(bool is_page);
};

}
#endif // REPPROJECT_H

