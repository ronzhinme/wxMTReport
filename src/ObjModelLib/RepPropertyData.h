
#ifndef PROPERTYDATA_H
#define PROPERTYDATA_H

#include <wx/wx.h>
#include <wx/propgrid/property.h>
#include "defines.h"
#include "PropNames.h"
#include "Utils.h"

class PropertyData
{
private:
  wxString prevdecpnt_;
  wxString ConvertFloatLocale(bool to_view);
public:
  bool is_enabled;
  long nam_id;
  wxVariant value;
  ePropertyType type;
  long cat_id;
  long choise_id;


  PropertyData();
  PropertyData(const PropertyData& d)
  {
    Copy(d);
  }
  PropertyData(long pid, wxVariant val, ePropertyType typ, long cat);
  ~PropertyData() {}

  wxPGChoices* GetChoices();
  bool GetColour(wxColour& colour);
  void SetColour(wxVariant colour_value);
  PropertyData& operator = (const PropertyData& d)
  {
    Copy(d);
    return *this;
  }
  wxString ToString();
  void FromString(const wxString data);
  void ConvertLocale();

private:
  void Copy(const PropertyData& d)
  {
    nam_id=d.nam_id;
    value=d.value;
    type=d.type;
    cat_id=d.cat_id;
    choise_id=d.choise_id;
    is_enabled=d.is_enabled;
    prevdecpnt_=d.prevdecpnt_;
  }
  wxArrayString GetChoisesLabels(wxString src_label);
  wxArrayInt GetChoisesValues(wxString src_vals);
};
class RepProperties
{
private:
  wxVector<PropertyData>* props_;
public:
  RepProperties();
  RepProperties(const RepProperties& repprops);
  ~RepProperties();
  void AddProperty(const PropertyData& propdt);
  bool DelProperty(wxString name);
  void DelProperty(long begin_indx, long end_indx);
  void Clear();
  void Append(RepProperties repprops);
  PropertyData* GetProperty(Property_ID id);
  PropertyData* GetProperty(long indx);
  long GetCount();
  wxString ToString();
  void FromString(const wxString from_filedata);
  void ConvertLocale();
};
#endif // PROPERTYDATA_H
