
#include "Element.h"
using namespace ElementModel;

//-----------------------------------------------------------------------------
//ctor
void Element::Init(wxString name,eRepElementType typ)
{
  PropertyData pdname(PROP_NAME_ID,name,PR_STRING,PCAT_COMMON_ID);
  PropertyData pdtype(PROP_TYPE_ID,typ,PR_SWITCH,PCAT_COMMON_ID);
  pdtype.choise_id = (long)REPORT_ELEMENT_ID;

  prop.AddProperty(pdname);
  prop.AddProperty(pdtype);
  id = wxNewId();
}
Element::Element(wxString name, eRepElementType typ)
{
  Init(name,typ);
}
Element::Element()
{
  Init(PropertyNames::GetNameByID(PROP_UNKNOWN_ITEM_ID),EL_UNKNOWN);
}
Element::Element(wxString name)
{
  Init(name,EL_UNKNOWN);
}
Element::Element(const Element& elbas)
{
  prop.Append(elbas.prop);
  id=elbas.id;
}
//-----------------------------------------------------------------------------

wxString Element::GetName()
{
  for(int f=0; f<prop.GetCount(); ++f)
  {
    PropertyData* pd = prop.GetProperty(f);
    if(pd==NULL) { continue; }
    if(pd->nam_id==PROP_NAME_ID)
    {
      return pd->value.GetString();
    }
  }
  return wxEmptyString;
}
eRepElementType Element::GetType()
{
  for(int f=0; f<prop.GetCount(); ++f)
  {
    PropertyData* pd = prop.GetProperty(f);
    if(pd==NULL) { continue; }
    if(pd->nam_id==PROP_TYPE_ID)
    {
      return (eRepElementType)pd->value.GetLong();
    }
  }
  return EL_NULL;
}
void Element::SetType(eRepElementType typ)
{
  for(int f=0; f<prop.GetCount(); ++f)
  {
    PropertyData* pd = prop.GetProperty(f);
    if(pd->nam_id==PROP_TYPE_ID)
    {
      if(pd==NULL) { continue; }
      if(pd->value.GetLong()==(long)typ) { return; }
      pd->value  = (long)typ;
      PropertyManager pm;
      pm.PrepareElementProperties(typ,prop);
      break;
    }
  }
}
void Element::SetName(wxString name)
{
  for(int f=0; f<prop.GetCount(); ++f)
  {
    PropertyData* pd = prop.GetProperty(f);
    if(pd==NULL) { continue; }
    if(pd->nam_id==PROP_NAME_ID)
    {
      if(pd->value.GetString().IsSameAs(name)) { return; }
      pd->value = name;
      break;
    }
  }
}
void Element::FromString(const wxString file_data)
{
  GetIDFromString(file_data);
  prop.FromString(file_data);
}
wxString Element::ToString()
{
  return DOCESECT_START+ IDToString() + prop.ToString() + DOCESECT_STOP;
}
void Element::GetIDFromString(const wxString file_data)
{
  wxString sect = GetSectionValue(file_data, ITEMIDSECT_START, ITEMIDSECT_STOP);
  if(!sect.IsEmpty())
    sect.ToLong((long*)&id);
}
wxString Element::IDToString()
{
  return ITEMIDSECT_START + wxString::FromDouble(id) + ITEMIDSECT_STOP;
}

void Element::ChangeSizeScale(double kf)
{
  for(int f=0; f<prop.GetCount(); ++f)
  {
    PropertyData* pd = prop.GetProperty(f);
    if(pd->cat_id != PCAT_POSITION_ID && pd->cat_id != PCAT_SIZE_ID) { continue; }
    pd->value = pd->value.GetDouble()*kf;
  }
}
