#include "RepPropertyData.h"

PropertyData::PropertyData()
{
  is_enabled=true;
}
PropertyData::PropertyData(long pid, wxVariant val, ePropertyType typ, long cat)
{
  nam_id = pid;
  value=val;
  type=typ;
  cat_id = cat;
  is_enabled=true;
}
wxArrayString PropertyData::GetChoisesLabels(wxString src_label)
{
  wxArrayString labels;
  if(src_label.IsNull() || src_label.IsEmpty())
  {
    return labels;
  }

  wxString tmp = src_label;
  int findpos = tmp.Find(CHOISES_DELIMETR);
  while(findpos!=wxNOT_FOUND)
  {
    wxString choise = tmp.Left(findpos);
    if(choise!=wxEmptyString)
    {
      labels.Add(choise);
    }
    tmp=tmp.Right(tmp.Len()-(findpos+1));
    findpos = tmp.Find(CHOISES_DELIMETR);
  }
  if(tmp!=wxEmptyString)
  {
    labels.Add(tmp);
  }
  return labels;
}
wxArrayInt PropertyData::GetChoisesValues(wxString src_vals)
{
  wxArrayInt values;
  if(src_vals.IsNull() || src_vals.IsEmpty())
  {
    return values;
  }
  wxArrayString strvals = GetChoisesLabels(src_vals);
  for(unsigned int f=0; f<strvals.size(); ++f)
  {
    long val=f;
    if(strvals.Item(f).ToLong(&val))
    {
      values.Add(val);
    }
  }
  return values;
}
wxPGChoices* PropertyData::GetChoices()
{
  wxPGChoices* choises = new wxPGChoices();
  if(choise_id <= SWITCH_LIST_ID || choise_id >= MAX_ID)
  {
    choise_id=-1;
    return NULL;
  }

  switch(choise_id)
  {
  case SIZESCALE_LABEL_ID:
    choises->Add(GetChoisesLabels(PropertyNames::GetNameByID(SIZESCALE_LABEL_ID)),GetChoisesValues(PropertyNames::GetNameByID(SIZESCALE_VALUE_ID)));
    break;
  case PAPERSIZE_LABEL_ID:
    choises->Add(GetChoisesLabels(PropertyNames::GetNameByID(PAPERSIZE_LABEL_ID)),GetChoisesValues(PropertyNames::GetNameByID(PAPERSIZE_VALUE_ID)));
    break;
  case PAPERORIENT_LABEL_ID:
    choises->Add(GetChoisesLabels(PropertyNames::GetNameByID(PAPERORIENT_LABEL_ID)),GetChoisesValues(PropertyNames::GetNameByID(PAPERORIENT_VALUE_ID)));
    break;
  default:
    choises->Add(GetChoisesLabels(PropertyNames::GetNameByID(choise_id)),GetChoisesValues(wxEmptyString));
    break;
  }
  return choises;
}
wxString PropertyData::ToString()
{
  wxString string_value=_T("");

  switch(type)
  {
  case PR_FONTSELECTOR:
    if(value.GetAny().CheckType<wxFont>())
    {
      string_value=value.GetAny().As<wxFont>().GetNativeFontInfoDesc ();
    }
    else if(value.GetAny().CheckType<wxString>())
    {
      string_value=value.GetString();
    }
    break;
  case PR_FLOAT:
  {
    ConvertFloatLocale(false);
    string_value=value.GetString();
  }
  break;
  default:
    string_value=value.GetString();
    break;
  }
  return _T(""+wxString::FromDouble(nam_id) +FIELD_DELIMETR+
            wxString::FromDouble(type)      +FIELD_DELIMETR+
            string_value                    +FIELD_DELIMETR+
            wxString::FromDouble(cat_id)    +FIELD_DELIMETR+
            wxString::FromDouble(choise_id) +FIELD_DELIMETR+
            wxString::FromDouble(is_enabled)+FIELD_DELIMETR+PROPERTY_DELIMETR);
}
void PropertyData::FromString(const wxString data)
{
  int fieldnum=0;
  wxString tmp = data;
  int findpos = tmp.Find(FIELD_DELIMETR);
  while(findpos!=wxNOT_FOUND)
  {
    wxString field = tmp.Left(findpos);
    switch(fieldnum++)
    {
    case 0://name
      field.ToLong(&nam_id);
      break;
    case 1://type
    {
      long typ=0;
      field.ToLong(&typ);
      type = (ePropertyType)typ;
      break;
    }
    case 2://value
      if(type==PR_FLOAT)
      {
        prevdecpnt_=_T(".");
        value = field;
        prevdecpnt_ = ConvertFloatLocale(true);
      }
      else
      {
        value = field;
      }
      break;
    case 3://category
      field.ToLong(&cat_id);
      break;
    case 4://choises_list
      field.ToLong(&choise_id);
      break;
    case 5://enabled
      long enb = 0;
      field.ToLong(&enb);
      is_enabled = enb;
      break;
    }
    tmp=tmp.Right(tmp.Len()-(findpos+1));
    findpos = tmp.Find(FIELD_DELIMETR);
  }
}
bool PropertyData::GetColour(wxColour& colour)
{
  colour.Set(value.GetLong());
  if(type!=PR_COLOR)
  {
    return false;
  }
  return true;
}
void PropertyData::SetColour(wxVariant colour_value)
{
  wxAny v = colour_value;
  wxColour colour;
  colour << v;
  value=(long)colour.GetRGBA();
}
void PropertyData::ConvertLocale()
{
  if(type==PR_FLOAT)
  {
    if(prevdecpnt_.IsEmpty())
    {
      wxLocale* loc = wxGetLocale();
      prevdecpnt_ = !loc?_T("."):loc->GetInfo(wxLOCALE_DECIMAL_POINT); //первоначальный
    }
    prevdecpnt_ = ConvertFloatLocale(true);
  }
}
wxString PropertyData::ConvertFloatLocale(bool to_view)
{
  wxLocale* loc = wxGetLocale();
  wxString decpoint = !loc?_T("."):loc->GetInfo(wxLOCALE_DECIMAL_POINT); //новый знак разделения

  if(decpoint.IsEmpty())
  {
    decpoint=_T(".");
  }

  wxString string_value=value.GetString();
  if(to_view)
  {
    string_value.Replace (prevdecpnt_,decpoint);
  }
  else
  {
    string_value.Replace (decpoint,_T("."));
  }

  value=string_value;
  return decpoint;
}
void RepProperties::AddProperty(const PropertyData& propdt)
{
  props_->push_back(propdt);
}
bool RepProperties::DelProperty(wxString name)
{
  for(int f=0; f<GetCount(); f++)
  {
    PropertyData* pd = GetProperty(f);
    if(pd->nam_id == PROP_NAME_ID)
    {
      props_->erase(props_->begin()+f);
      return true;
    }
  }
  return false;
}
void RepProperties::DelProperty(long begin_indx, long end_indx)
{
  if(begin_indx<0)
  {
    begin_indx=0;
  }
  if(end_indx>GetCount())
  {
    end_indx=GetCount();
  }

  props_->erase(props_->begin()+begin_indx,props_->begin()+end_indx);
}
PropertyData* RepProperties::GetProperty(long indx)
{
  if(indx<0 || indx>=GetCount())
  {
    return NULL;
  }
  return &(props_->at(indx));
}
PropertyData* RepProperties::GetProperty(Property_ID id)
{
  for(long f=0; f<GetCount(); ++f)
  {
    PropertyData* pd = GetProperty(f);
    if(pd==NULL)
    {
      continue;
    }
    if(pd->nam_id==(long)id)
    {
      return pd;
    }
  }
  return NULL;
}
long RepProperties::GetCount()
{
  return props_->size();
}
wxString RepProperties::ToString()
{
  wxString res = PROPSECT_START;
  wxVector<PropertyData>::iterator i;
  for(i=props_->begin(); i!=props_->end(); ++i)
  {
    res+=(*i).ToString();
  }
  res+=PROPSECT_STOP;
  return res;
}
void RepProperties::FromString(const wxString from_filedata)
{
  Clear();
  if(from_filedata==wxEmptyString)
  {
    return;
  }

  wxString data = from_filedata;
  wxString tmp = GetSectionValue(data, PROPSECT_START, PROPSECT_STOP);
  data.Replace(tmp,"");
  int findposbeg = tmp.Find(PROPERTY_DELIMETR);
  while(findposbeg!=wxNOT_FOUND)
  {
    wxString propstring = tmp.Left(findposbeg);
    tmp = tmp.Remove(0,findposbeg+1);
    PropertyData dt;
    dt.FromString(propstring);
    AddProperty(dt);
    findposbeg = tmp.Find(PROPERTY_DELIMETR);
  }
}
RepProperties::RepProperties()
{
  props_ = new wxVector<PropertyData>();
}
RepProperties::RepProperties(const RepProperties& repprops)
{
  props_ = new wxVector<PropertyData>();

  RepProperties* p = (RepProperties*)&repprops;
  for(int f=0; f<p->GetCount(); ++f)
  {
    props_->push_back( *p->GetProperty(f) );
  }
}
RepProperties::~RepProperties()
{
  Clear();
  delete props_;
}
void RepProperties::Clear()
{
  props_->clear();
  props_->swap(*props_);
}
void RepProperties::Append(RepProperties repprops)
{
  for(int f=0; f<repprops.GetCount(); ++f)
  {
    props_->push_back( *repprops.GetProperty(f) );
  }
}
void RepProperties::ConvertLocale()
{
  wxVector<PropertyData>::iterator i;
  for(i=props_->begin(); i!=props_->end(); ++i)
  {
    (*i).ConvertLocale();
  }
}
