
#ifndef REPVAR_H
#define REPVAR_H

#include <wx/vector.h>
#include "defines.h"
struct Var
{
  wxString name;
  wxString* value;
  bool is_used;

  Var(wxString n)
  {
    value=new wxString();
    name=n;
    is_used=false;
  }
  void SetVal(wxString val)
  {
    *value = _T(""+val);
#ifdef LOG
    wxString msg;
    msg.Printf("Set var %s = %s\n",*value,val);
    WRITELOG(msg);
#endif
  }
};

class RepVars
{
private:
  wxVector<Var> vars_;

public:
  RepVars() {}
  ~RepVars() {}

  Var* FindVar(wxString varname);
  bool AddVar(wxString varname);
  bool DelVar(wxString varname);
  void Clear();

  wxVector<Var> GetVars();
  wxString ToString();
  int SetVars(wxString s);
  int SetVars(wxVector<Var> s);
};

#endif // REPVAR_H
