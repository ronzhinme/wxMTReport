
#include <wx/wx.h>

#ifndef UTILSREPPROJECT_H
#define UTILSREPPROJECT_H

wxString GetSection(wxString text, wxString tag_beg, wxString tag_end);
wxString GetSectionValue(wxString text, wxString tag_beg, wxString tag_end);
#endif //UTILSREPPROJECT_H

