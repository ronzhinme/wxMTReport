
#ifndef DOCUMENTS_H
#define DOCUMENTS_H

#include <wx/cmndata.h>
#include "ElementsTemplate.h"

namespace ElementModel
{

class Doc:public Element
{
public :
  Elements<Element> elems;
  wxPrintData print_dt;
  wxSize sizemm;

  Doc();
  Doc(wxString name);
  Doc(wxString name, eRepElementType typ);
  Doc(const Element& elbas);
  ~Doc();
  void FromString(const wxString file_data);
  wxString ToString();
  void ChangeSizeScale(long scale_val);
  void ChangePaperSize(long paper_id);
  void ChangePaperOrient(long orient_id);
private:
  void Init();
  void SetPaperConfig();
  bool GetSizeScaleKoef(double &koef,double &pkoef);
};

}
#endif // DOCUMENTS_H

